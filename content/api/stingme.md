---
author: Татьяна Соколова
categories:
- Апитерапия
- Видео
color: red
date: 2013-01-21T16:22:49+00:00
description: Учебныe фильмы о применении апитерапии
img: sting.jpg
menu:
  main:
    parent: api
    weight: 15
slug: apimovies
tags:
- видео
title: Учебные фильмы
type: post
weight: 10
---

#### Ужальте меня на здоровье

{{% youtube siHO2VEoRS0 %}}

___
#### Лечение пчелами. Запись трансляции, 1 канал

{{% youtube 0WjuYPKo6TE %}}


___
#### Апитерапия в СССР

Фильм о развитии апитерапии в СССР. Историческая справка

{{% youtube OndhVLFJCmg %}}

___
#### Лечим укусами пиявки, личинки и пчелы

Запись трансляции канала Дискавери, 2000

{{% youtube 1ZGwvg3Ahe8 %}}

