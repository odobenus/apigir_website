---
author: Татьяна Соколова
categories:
- Апитерапия
color: red
date: 2013-04-17T15:34:32+00:00
description: Статья о применении и целебных свойствах прополиса
img: propolis.jpg
menu:
  main:
    parent: api
    weight: 10
slug: propolis
title: Антибиотики и прополис
type: post
weight: 4
---

В середине прошлого века медицина получила в свое распоряжение великолепное средство, которое, как в то время казалось, навсегда
решает проблему инфекций — антибиотики. Лечебный эффект антибиотиков основан на способности одних микробов выделять специальные
вещества для защиты от других микробов или для нападения на них. Эти вещества являются естественными продуктами
жизнедеятельности микроорганизмов. То есть, ученые научились выделять специфические вещества из одних микроорганизмов для борьбы
с другими микроорганизмами.
<!--more-->

{{< kart src="propolis.jpg" side="left" >}} 

Казалось бы, все замечательно. Однако, если в начале использования антибиотиков, суточная эффективная лечебная доза пенициллина,
к примеру, составляла 1000 — 5000 единиц, то современная средняя суточная доза – 1000000-2000000 единиц, кроме того, пенициллин
уже не так эффективен. То есть, микробы научились вырабатывать вещества, которые нейтрализуют действие пенициллина, и это
свойство адаптации патогенных микробов к антибиотикам приводит к снижению их эффективности. Значит требуются все большие дозы
или новые виды препаратов для лечения. В то же время, многообразие микроорганизмов в природе огромно. Можно находить, выделять и
очищать новые и более сильные виды антибиотиков для лечения различных инфекций, и работы в этом направлении ведутся постоянно.
Но все чаще слышны голоса ученых и врачей, которые предостерегают от опасности расширяющегося использования антибиотиков.

Не надо забывать, что от антибиотиков, наряду с «плохими», гибнут и «хорошие» микроорганизмы, например те, что составляют
нормальную флору кишечника. Помимо гибели микроорганизмов, антибиотики влияют на здоровые клетки организма, снижая в них
выработку энергии, поскольку отрицательно воздействуют на клеточные &#171;электростанции&#187; &#8212; митохондрии. Наука только
сейчас подошла к пониманию роли митохондрий в развитии большинства заболеваний.

И, как обычно, когда первая волна эйфории от нового проходит и проявляются побочные воздействия, человек вспоминает о том, что
есть веками проверенные естественные средства борьбы с инфекцией. Эти средства не предназначены для экстренных ситуаций,
требующих немедленной госпитализации, но вполне могут быть использованы в случаях хронических заболеваний и легких нарушений
здоровья, могут применяться в качестве профилактики и в восстановительном периоде после перенесенных заболеваний.

Апитерапия располагает великолепным естественным средством борьбы с патогенными микроорганизмами - *ПРОПОЛИСОМ*.

О замечательных свойствах прополиса, наверняка, наслышан каждый. Прополис — это смолистый продукт, призводимый пчелами на основе
сборов с различных растений. Это сильно клейкие смолистые вещества, собираемые, трансформируемые, и используемые пчелами для
герметизации отверстий в сотах, для стерилизации ячеек сот, сглаживания внутренних стенок улья, покрытия тел посторонних
организмов, которые умерли в улье, чтобы избежать их разложения. Прополис защищает пчелиную колонию от болезней, потому что он
обладает антисептическими, антимикробными свойствами.

Эти замечательные свойства прополиса активно используются традиционной медициной уже не одно тысячелетие.

В настоящее время биологические свойства и широкий спектр фармакологического действия прополиса исследуются как в российских
научных институтах, так и в лабораториях всего мира.

Прополис оказывается эффективен в случаях заболеваний печени и почек, при лечении обширных ожогов и заживлении ран, как
стимулятор иммунной системы и нативный антибиотик, положительно действует при использовании в случаях сахарного диабета и
онкологическх заболеваний (подавляет рост раковых клеток, не являясь при этом токсичным) и т. д. (ссылочка на кусок из «Прополис
для реферата»)

Прополис действует слабее ныне использующихся сильных антибиотиков, но он менее токсичен и **_не вызывает приспосабливаемости у микроорганизмов_**, поэтому его можно использовать длительно при хронических процессах. Кроме
того, в отличие от антибиотиков, прополис **_не угнетает нормальную кишечную флору_**, не вызывает дисбактериоз.

В лечении используются разные лекарственные формы прополиса, такие как водный и спиртовый экстракты, мази, свечи, ингалляции,
настойки.

Как и для большинства лекарственных препаратов, существуют противопоказания для лечения прополисом. Такими противопоказаниями
являются аллергические реакции и некоторые заболевания. Только врач-апитерапевт может определить целесообразность применения
прополиса при тех или иных заболеваниях, наиболее оптимальную дозу и способ использования.

