---
author: Татьяна Соколова
categories:
- Апитерапия
color: red
date: 2013-05-03T15:18:35+00:00
description: Справочная информация
menu:
  main:
    parent: api
    weight: 10
slug: precau
tags:
- противопоказания
- показания
title: Показания и противопоказания
type: post
weight: 8
---

##### Показания к пчелоужалению

Заб. нервной системы 
: остеохондрозы, невриты и невралгии; последствия травмы; острый ишемический инсульт и его последствия; мигрень; паркинсонизм; атеросклероз сосудов головного мозга; параличи и парезы; болевой синдром различной природы; неврозы, депрессии. 

Кардиология 
: инфаркт миокарда и его последствия; ИБС; стенокардия; аритмии; гипертензия и гипотензия; тромбофлебит сосудов нижних конечностей; облитерирующий эндартериит; варикозное расширение вен.
 <!--more-->

Пульмонология
: бронхиальная астма, хронический бронхит.

Психиатрия
: алкоголизм; табакокурение; наркомания; токсикомании;

Гастроэнтерология
: язвенная болезнь желудка и двенадцатиперстной кишки, исключая период обострения; колиты, гастриты,  желчнокаменная болезнь, холециститы; хронический геморрой.

Эндокринные и болезни обмена веществ
: сахарный диабет 2-го типа; тиреотоксикоз; ожирение.

Ревматология
: артириты и  полиартриты;  спондилоартрит; деформирующий остеоартроз; подагра; миалгии; миозиты; мышечные контрактуры, в т.ч. после ожогов.

Инфекционные, хронические инфекции
: рожа; герпес; остаточные явления гриппа.

Хирургия
: облитерирующий атеросклероз и эндоартериит; хронические язвы и раны; пролежни; послеоперационные рубцы.

Урология и гинекология
: почечнокаменная болезнь; хронический простатит; импотенция; аденома простаты; патологический климакс; трубное и гормональное бесплодие; нарушения менструального цикла.

Дерматология
: экзема, псориаз, зудящие дерматозы; нейродермит; дерматиты.

Иммунные нарушения
: патология микроциркуляции.

Геронтология
: старческое увядание, преждевременное старение.



##### Противопоказания

-  Болезнь Аддисона
-  аллергические реакции;
-  туберкулез в активной фазе;
-  сахарный диабет I типа;
-  онкологические болезни;
-  острые инфекционные заболевания;
-  беременность;
-  возраст до 14 лет.

