---
author: walrus
categories:
- Рефлексотерапия
- Видео
color: black
date: 2014-10-06T21:29:00Z
description: Учебный фильм
img: spina.jpg
linktitle: Упражнения для спины
menu:
  main:
    parent: reflex
    weight: 10
slug: spina
tags:
- видео
title: Упражнения при болях в позвоночнике
weight: 20
---

{{% youtube LrURE7cftZw %}}

