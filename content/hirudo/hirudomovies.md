---
author: Татьяна Соколова
categories:
- Гирудотерапия
- Видео
color: blue
date: 2013-02-21T17:00:18+00:00
description: о применении гирудотерапии
img: nauka2.jpg
menu:
  main:
    parent: hirudo
    weight: 10
slug: hirudomovies
tags:
- видео
title: Учебные фильмы (гирудо)
type: post
---

#### Наука 2 O Пиявках

{{% youtube NDDxKxjaeeQ %}}

----

#### Маленькое существо и огромная польза: что способна дать человеку пиявка?

{{% youtube pT1m1bWOmGw %}}

----

#### Гирудотерапия — Лечение пиявками

{{% youtube g9JjYgQlcH8 %}} 
